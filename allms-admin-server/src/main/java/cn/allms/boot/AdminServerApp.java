package cn.allms.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xieya
 * @date 2021/8/16
 */
@SpringBootApplication
@RestController
public class AdminServerApp {
    public static void main(String[] args) {
        SpringApplication.run(AdminServerApp.class, args);
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }
}
